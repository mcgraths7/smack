//
//  AvatarCell.swift
//  smack
//
//  Created by Steven McGrath on 9/7/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class AvatarCell: UICollectionViewCell {

    // MARK: IBOutlets
    @IBOutlet weak var avatarImage: UIImageView!

    // MARK: Setup
    func updateViews() {
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }

    func configureCell(at index: Int, withType type: AvatarType) {
        if type == AvatarType.dark {
            avatarImage.image = UIImage(named: "dark\(index)")
            self.layer.backgroundColor = UIColor.lightGray.cgColor
        } else {
            avatarImage.image = UIImage(named: "light\(index)")
            self.layer.backgroundColor = UIColor.gray.cgColor
        }
    }

    // MARK: Override Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        updateViews()
    }
}

enum AvatarType {
    case dark, light
}
