//
//  RoundedButton.swift
//  Smack
//
//  Created by Jonny B on 7/17/17.
//  Copyright © 2017 Jonny B. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedButton: UIButton {

    // MARK: Inspectable Variables
    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }

    // MARK: Functions
    func setupView() {
        self.layer.cornerRadius = cornerRadius
    }

    // MARK: Overridden Functions
    override func awakeFromNib() {
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
}
