//
//  ChannelCell.swift
//  smack
//
//  Created by Steven McGrath on 9/9/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class ChannelCell: UITableViewCell {

    @IBOutlet weak var channelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2999518408)
        } else {
            self.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        }
    }

    func configureCell(channel: Channel) {
        let name = channel.channelTitle ?? ""
        channelName.text = name
    }

}
