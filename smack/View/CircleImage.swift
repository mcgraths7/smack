//
//  CircleImage.swift
//  smack
//
//  Created by Steven McGrath on 9/8/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

@IBDesignable
class CircleImage: UIImageView {

    // MARK: Setup
    func setupView() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }



    // MARK: Override Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
    }



}
