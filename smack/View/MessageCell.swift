//
//  MessageCell.swift
//  smack
//
//  Created by Steven McGrath on 9/12/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    // MARK: IBOutlets
    @IBOutlet weak var profileAvatar: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(message: Message) {
        messageLabel.text = message.message
        usernameLabel.text = message.userName
//        timestampLabel.text = message.timeStamp
        profileAvatar.image = UIImage(named: message.userAvatar)
        profileAvatar.backgroundColor = UserDataService.instance.getUIColor(components: message.userAvatarColor)
    }

}
