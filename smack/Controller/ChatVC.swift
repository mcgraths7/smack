//
//  ChatVC.swift
//  smack
//
//  Created by Steven McGrath on 9/7/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: IBOutlets
    @IBOutlet weak var smackBurger: UIButton!
    @IBOutlet weak var channelNameLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var messagesTable: UITableView!

    // MARK: IBActions
    @IBAction func onSendTapped(_ sender: UIButton) {
        if AuthService.instance.isLoggedIn {
            guard let channelId = MessageService.instance.selectedChannel?.id else { return }
            guard let message = messageTextField.text else { return }
            SocketService.instance.addMessage(messageBody: message, userId: UserDataService.instance._id, channelId: channelId) { (success) in
                if success {
                    self.messageTextField.text = ""
                    self.messageTextField.resignFirstResponder()
                }
            }
        }
        messagesTable.reloadData()
    }

    // MARK: Table view methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.messages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = messagesTable.dequeueReusableCell(withIdentifier: MESSAGE_CELL, for: indexPath) as? MessageCell {
            let message = MessageService.instance.messages[indexPath.row]
            cell.configureCell(message: message)
            return cell
        } else {
            return MessageCell()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }



    // MARK: Selector Functions
    @objc func userDataDidChange(_ notif: Notification) {
        if AuthService.instance.isLoggedIn {
            onLoginGetMessages()
        } else {
            channelNameLabel.text = "Please log in"
        }
    }

    @objc func channelSelected(_notif: Notification) {
        updateWithChannel()
    }

    @objc func handleTap() {
        view.endEditing(true)
    }

    // MARK: Message and Channel Handling
    func onLoginGetMessages() {
        MessageService.instance.findAllChannels { (success) in
            if success {
                if !MessageService.instance.channels.isEmpty {
                    MessageService.instance.selectedChannel = MessageService.instance.channels[0]
                    self.updateWithChannel()
                } else {
                    self.channelNameLabel.text = "No channels yet..."
                }
            }
        }
    }

    func updateWithChannel() {
        channelNameLabel.text = MessageService.instance.selectedChannel?.channelTitle ?? ""
        getMessages()
    }

    func getMessages() {
        guard let channelId = MessageService.instance.selectedChannel?.id else { return }

        MessageService.instance.findAllMessagesForChannel(channelId: channelId) { (success) in
            if success {
                self.messagesTable.reloadData()
            }
        }
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        messagesTable.delegate = self
        messagesTable.dataSource = self
        messagesTable.estimatedRowHeight = 80
        messagesTable.rowHeight = UITableView.automaticDimension
    }

    // MARK: Set up view
    func setupView() {
        smackBurger.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())

        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.userDataDidChange(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.channelSelected(_notif:)), name: NOTIF_CHANNEL_SELECTED, object: nil)

        if AuthService.instance.isLoggedIn {
            AuthService.instance.findUserByEmail { (success) in
                if success {
                    NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                }
            }
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(ChatVC.handleTap))
        view.addGestureRecognizer(tap)
        view.bindToKeyboard()
    }
}
