//
//  ProfileVC.swift
//  smack
//
//  Created by Steven McGrath on 9/9/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var profileAvatar: UIImageView!
    @IBOutlet weak var bgView: UIView!

    // MARK: IBActions
    @IBAction func onLogoutTapped(_ sender: UIButton) {
        UserDataService.instance.logoutUser()
        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onCloseModalTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: Setup View
    func setupView() {
        usernameLbl.text = UserDataService.instance.name
        emailLbl.text = UserDataService.instance.email
        profileAvatar.image = UIImage(named: UserDataService.instance.avatarName)
        profileAvatar.backgroundColor = UserDataService.instance.getUIColor(components: UserDataService.instance.avatarColor)

        let closeTap = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.closeOnTap(_:)))
        bgView.addGestureRecognizer(closeTap)
    }

    @objc func closeOnTap(_ recognizer: UITapGestureRecognizer) {
        print("!")
        dismiss(animated: true, completion: nil)
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
}
