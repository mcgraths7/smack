//
//  LoginVC.swift
//  smack
//
//  Created by Steven McGrath on 9/7/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    // MARK: IBActions
    @IBAction func onCancelButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func onSignupButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: TO_SIGNUP, sender: nil)
    }

    @IBAction func onLoginButtonTapped(_ sender: RoundedButton) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()

        guard let email = emailTxt.text, emailTxt.text != "" else { return }
        guard let password = passwordTxt.text, passwordTxt.text != "" else { return }

        AuthService.instance.loginUser(email: email, password: password) { (success) in
            if success {
                AuthService.instance.findUserByEmail(completion: { (success) in
                    if success {
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
        }
    }

    // MARK: Setup View
    func setupView() {
        activityIndicator.isHidden = true

        emailTxt.attributedPlaceholder = ColorService.instance.adjustPlaceholderColor(for: "email")
        passwordTxt.attributedPlaceholder = ColorService.instance.adjustPlaceholderColor(for: "password")

        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginVC.handleTap))
        view.addGestureRecognizer(tap)
    }

    @objc func handleTap() {
        view.endEditing(true)
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
}
