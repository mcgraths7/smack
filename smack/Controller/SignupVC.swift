//
//  SignupVC.swift
//  smack
//
//  Created by Steven McGrath on 9/7/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var profileAvatar: UIImageView!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var createAccountBtn: RoundedButton!

    // MARK: Variables
    var avatarName = "profileDefault.png"
    var avatarColor = "[0.5,0.5,0.5,1]"
    var bgColor: UIColor!

    // MARK: IBActions
    @IBAction func onCancelButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: UNWIND, sender: nil)
    }

    @IBAction func onChooseAvatarTapped(_ sender: UIButton) {
        performSegue(withIdentifier: TO_AVATARS, sender: nil)

    }

    @IBAction func onGenerateBGColorTapped(_ sender: UIButton) {
        let color = ColorService.instance
        bgColor = color.getUIColor()
        let rgba = color.getRGBAComponents(color: bgColor)
        UIView.animate(withDuration: 0.3) {
            self.profileAvatar.backgroundColor = self.bgColor
        }
        avatarColor = "[\(rgba.red),\(rgba.green),\(rgba.blue),\(rgba.alpha)]"
    }

    @IBAction func onCreateAccountTapped(_ sender: UIButton) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        createAccountBtn.isEnabled = false
        guard let email =  emailTxt.text , emailTxt.text != "" else { return }
        guard let password = passwordTxt.text , passwordTxt.text != "" else { return }
        guard let name = usernameTxt.text, usernameTxt.text != "" else { return }

        AuthService.instance.registerUser(email: email, password: password) { (success) in
            if success {
                AuthService.instance.loginUser(email: email, password: password, completion: { (success) in
                    if success {
                        AuthService.instance.createUser(name: name, email: email, avatarName: self.avatarName, avatarColor: self.avatarColor, completion: { (success) in
                            if success {
                                self.activityIndicator.stopAnimating()
                                self.activityIndicator.isHidden = true
                                self.createAccountBtn.isEnabled = true
                                self.performSegue(withIdentifier: UNWIND, sender: nil)
                                NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)

                            }
                        })
                    }
                })
            }
        }
    }

    // MARK: Setup
    func setupView() {
        activityIndicator.isHidden = true

        usernameTxt.attributedPlaceholder = ColorService.instance.adjustPlaceholderColor(for: "username")
        emailTxt.attributedPlaceholder = ColorService.instance.adjustPlaceholderColor(for: "email")
        passwordTxt.attributedPlaceholder = ColorService.instance.adjustPlaceholderColor(for: "password")

        let tap = UITapGestureRecognizer(target: self, action: #selector(SignupVC.handleTap))
        view.addGestureRecognizer(tap)
    }

    @objc func handleTap() {
        view.endEditing(true)
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewDidAppear(_ animated: Bool) {
        if UserDataService.instance.avatarName != "" {
            profileAvatar.image = UIImage(named: UserDataService.instance.avatarName)
            avatarName = UserDataService.instance.avatarName
            if avatarName.hasPrefix("dark") {
                profileAvatar.backgroundColor = UIColor.lightGray
            } else {
                profileAvatar.backgroundColor = UIColor.gray
            }
        }
    }
}
