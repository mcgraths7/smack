//
//  CreateChannelVC.swift
//  smack
//
//  Created by Steven McGrath on 9/9/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class CreateChannelVC: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var channelNameTxt: UITextField!
    @IBOutlet weak var channelDescriptionTxt: UITextField!

    // MARK: IBActions
    @IBAction func onCloseModalTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func onCreateChannelTapped(_ sender: RoundedButton) {
        guard let channelName = channelNameTxt.text , channelNameTxt.text != "" else { return }
        guard let channelDescription = channelDescriptionTxt.text else { return }

        SocketService.instance.addChannel(channelName: "@\(channelName)", channelDescription: channelDescription, completion: { (success) in
            if success {
                self.dismiss(animated: true, completion: nil)
            }
        })
    }

    // MARK: Setup View
    func setupView() {
        channelNameTxt.attributedPlaceholder = ColorService.instance.adjustPlaceholderColor(for: "name")
        channelDescriptionTxt.attributedPlaceholder = ColorService.instance.adjustPlaceholderColor(for: "description")
        let tap = UITapGestureRecognizer(target: self, action: #selector(CreateChannelVC.handleTap))
        view.addGestureRecognizer(tap)
    }

    @objc func handleTap() {
        view.endEditing(true)
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
}
