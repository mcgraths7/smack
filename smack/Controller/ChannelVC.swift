//
//  ChannelVC.swift
//  smack
//
//  Created by Steven McGrath on 9/7/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController, UITableViewDataSource, UITableViewDelegate {


    // MARK: IBOutlets
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var profileAvatar: UIImageView!
    @IBOutlet weak var channelsTable: UITableView!
    @IBOutlet weak var addChannelButton: UIButton!

    // Mark: Actions

    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) { }

    @IBAction func onLoginButtonTapped(_ sender: UIButton) {
        if AuthService.instance.isLoggedIn {
            let profile = ProfileVC()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true, completion: nil)
        } else {
            performSegue(withIdentifier: TO_LOGIN, sender: nil)
        }
    }

    @IBAction func onAddChannelTapped(_ sender: UIButton) {
        let addChannel = CreateChannelVC()
        addChannel.modalPresentationStyle = .custom
        present(addChannel, animated: true, completion: nil)
    }

    // MARK: Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.channels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CHANNEL_CELL, for: indexPath) as? ChannelCell {
            let channel = MessageService.instance.channels[indexPath.row]
            cell.configureCell(channel: channel)
            return cell
        } else {
            return ChannelCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = MessageService.instance.channels[indexPath.row]
        MessageService.instance.selectedChannel = channel
        NotificationCenter.default.post(name: NOTIF_CHANNEL_SELECTED, object: nil)

        self.revealViewController().revealToggle(animated: true)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.width - 65
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelVC.userDataDidChange(_:)), name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelVC.onChannelsLoaded(_notif:)), name: NOTIF_CHANNELS_LOADED, object: nil)
        channelsTable.delegate = self
        channelsTable.dataSource = self
        SocketService.instance.getChannel { (success) in
            if success {
                self.channelsTable.reloadData()
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
        setupUserInfo()
    }

    // MARK: Observer Functions
    @objc func userDataDidChange(_ notif: Notification) {
        setupUserInfo()
    }

    @objc func onChannelsLoaded(_notif: Notification) {
        channelsTable.reloadData()
    }

    func setupUserInfo() {
        if !AuthService.instance.isLoggedIn {
            addChannelButton.isHidden = true
        } else {
            addChannelButton.isHidden = false
        }
        if AuthService.instance.isLoggedIn {
            loginButton.setTitle(UserDataService.instance.name, for: .normal)
            profileAvatar.image = UIImage(named: UserDataService.instance.avatarName)
            profileAvatar.backgroundColor = UserDataService.instance.getUIColor(components: UserDataService.instance.avatarColor)
        } else {
            loginButton.setTitle("Login", for: .normal)
            profileAvatar.image = UIImage(named: "menuProfileIcon")
            profileAvatar.backgroundColor = UIColor.clear
            channelsTable.reloadData()
        }
    }
}
