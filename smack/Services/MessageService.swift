//
//  MessageService.swift
//  smack
//
//  Created by Steven McGrath on 9/9/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MessageService {
    static let instance = MessageService()

    var channels = [Channel]()
    var selectedChannel: Channel?
    var messages = [Message]()

    func findAllChannels(completion: @escaping CompletionHandler) {
        Alamofire.request(DEV_GET_CHANNELS_URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else { return }
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let name = item["name"].stringValue
                            let description = item["description"].stringValue
                            let id = item["_id"].stringValue
                            let channel = Channel(channelTitle: name, channelDescription: description, id: id)
                            self.channels.append(channel)
                        }
                        NotificationCenter.default.post(name: NOTIF_CHANNELS_LOADED, object: nil)
                    }
                    completion(true)
                } catch let error as NSError {
                    debugPrint(error as Any)
                    completion(false)
                }
            }
        }
    }

    func findAllMessagesForChannel(channelId: String, completion: @escaping CompletionHandler) {
        Alamofire.request("\(DEV_GET_MESSAGES_URL)/\(channelId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                self.clearMessages()

                do {
                    guard let data = response.data else { return }
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let id = item["_id"].stringValue
                            let messageBody = item["messageBody"].stringValue
                            let userId = item["userId"].stringValue
                            let channelId = item["channelId"].stringValue
                            let userName = item["userName"].stringValue
                            let userAvatar = item["userAvatar"].stringValue
                            let userAvatarColor = item["userAvatarColor"].stringValue
                            let timeStamp = item["timeStamp"].stringValue

                            let message = Message(message: messageBody, userId: userId, channelId: channelId, userName: userName, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: id, timeStamp: timeStamp)
                            print(self.messages)
                            self.messages.append(message)
                        }
                        completion(true)
                    }
                } catch let error as NSError {
                    debugPrint(error as Any)
                }
            } else {
                debugPrint(response.result.error as Any)
                completion(false)
            }
        }
    }

    func clearChannels() {
        channels.removeAll()
    }

    func clearMessages() {
        messages.removeAll()
    }


}
