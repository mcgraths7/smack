//
//  RandomColor.swift
//  smack
//
//  Created by Steven McGrath on 9/8/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class ColorService {

    static let instance = ColorService()

    private(set) public var red: CGFloat = 0
    private(set) public var blue: CGFloat = 0
    private(set) public var green: CGFloat = 0
    private(set) public var alpha: CGFloat = 1

    func getUIColor() -> UIColor {
        self.red = CGFloat(arc4random_uniform(255)) / 255
        self.green = CGFloat(arc4random_uniform(255)) / 255
        self.blue = CGFloat(arc4random_uniform(255)) / 255
        self.alpha = CGFloat(1)
        return UIColor.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    func adjustPlaceholderColor(for placeholder: String) -> NSAttributedString {
        return NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: SMACK_PURPLE_PLACEHOLDER])
    }



    //  UIColor+Components.swift
    //
    //  Copyright (c) 2015 Stefan Jager
    //  Permission is hereby granted, free of charge, to any person obtaining a
    //  copy of this software and associated documentation files (the "Software"),
    //  to deal in the Software without restriction, including without limitation
    //  the rights to use, copy, modify, merge, publish, distribute, sublicense,
    //  and/or sell copies of the Software, and to permit persons to whom the
    //  Software is furnished to do so, subject to the following conditions:
    //
    //  The above copyright notice and this permission notice shall be included in
    //  all copies or substantial portions of the Software.
    //
    //  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    //  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    //  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    //  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    //  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    //  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    //  DEALINGS IN THE SOFTWARE.
    //
    //
    // I have modified the code for my needs but I still wanted to give credit to the original author of the code below
    
    func getRGBAComponents(color: UIColor) -> (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)
    {
        var (red, green, blue, alpha) = (CGFloat(0.0), CGFloat(0.0), CGFloat(0.0), CGFloat(0.0))
        if color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        {
            return (red, green, blue, alpha)
        }
        else
        {
            return (0.5, 0.5, 0.5, 1)
        }
    }
}

