//
//  UserDataService.swift
//  smack
//
//  Created by Steven McGrath on 9/8/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation

class UserDataService {
    static let instance = UserDataService()

    private(set) public var _id = ""
    private(set) public var email = ""
    private(set) public var name = ""
    private(set) public var avatarName = ""
    private(set) public var avatarColor = ""

    func setUserData(_id: String, email: String, name: String, avatarName: String, avatarColor: String) {
        self._id = _id
        self.email = email
        self.name = name
        self.avatarName = avatarName
        self.avatarColor = avatarColor
    }

    func setAvatarName(avatarName: String) {
        self.avatarName = avatarName
    }

    func getUIColor(components: String) -> UIColor{
        let defaultColor = UIColor.lightGray

        // Scanner setup
        let scanner = Scanner(string: components)
        let skipped = CharacterSet(charactersIn: "[], ")
        let comma = CharacterSet(charactersIn: ",")
        scanner.charactersToBeSkipped = skipped

        // Initialize variables as optional NSString - required by scanner
        var r, g, b, a: NSString?

        // Scan up to the comma, as defined above, and dump the results into a variable preceeded by "&"
        scanner.scanUpToCharacters(from: comma, into: &r)
        scanner.scanUpToCharacters(from: comma, into: &g)
        scanner.scanUpToCharacters(from: comma, into: &b)
        scanner.scanUpToCharacters(from: comma, into: &a)

        // Unwrap Optionals, otherwise return default color
        guard let rUnwrapped = r else { return defaultColor }
        guard let gUnwrapped = g else { return defaultColor }
        guard let bUnwrapped = b else { return defaultColor }
        guard let aUnwrapped = a else { return defaultColor }

        // Convert to CGFloat, as required by UIColor Initializer
        let rFloat = CGFloat(rUnwrapped.doubleValue)
        let gFloat = CGFloat(gUnwrapped.doubleValue)
        let bFloat = CGFloat(bUnwrapped.doubleValue)
        let aFloat = CGFloat(aUnwrapped.doubleValue)

        return UIColor(red: rFloat, green: gFloat, blue: bFloat, alpha: aFloat)
    }

    func logoutUser() {
        _id = ""
        avatarColor = "[0.5, 0.5, 0.5, 1]"
        avatarName = ""
        email = ""
        name = ""
        AuthService.instance.isLoggedIn = false
        AuthService.instance.authToken = ""
        AuthService.instance.userEmail = ""
        MessageService.instance.clearChannels()
    }

}
