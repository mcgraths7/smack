//
//  RandomColorService.swift
//  smack
//
//  Created by Steven McGrath on 9/8/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class ColorService: UIColor {

    private var red: CGFloat
    private var green: CGFloat
    private var blue: CGFloat

    func randomColor() -> UIColor {
        return UIColor.init(red: red, green: green, blue: blue, alpha: 1)
    }
}
