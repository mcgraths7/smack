//
//  AuthService.swift
//  smack
//
//  Created by Steven McGrath on 9/8/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService {
    static let instance = AuthService()

    let  defaults = UserDefaults.standard

    var isLoggedIn: Bool {
        get {
            return defaults.bool(forKey: LOGGED_IN_KEY)
        }
        set {
            defaults.set(newValue, forKey: LOGGED_IN_KEY)
        }
    }

    var authToken: String {
        get {
            return defaults.value(forKey: TOKEN_KEY) as! String
        }
        set {
            defaults.set(newValue, forKey: TOKEN_KEY)
        }
    }

    var userEmail: String {
        get {
            return defaults.value(forKey: USER_EMAIL_KEY) as! String
        }
        set {
            defaults.set(newValue, forKey: USER_EMAIL_KEY)
        }
    }

    func registerUser(email: String, password: String, completion: @escaping CompletionHandler) {
        let lowercaseEmail = email.lowercased()
        let body: [String: Any] = [
            "email": lowercaseEmail,
            "password": password
        ]

        Alamofire.request((DEV_REGISTER_USER_URL), method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }

    func loginUser(email: String, password: String, completion: @escaping CompletionHandler) {
        let lowercaseEmail = email.lowercased()

        let body: [String: Any] = [
            "email": lowercaseEmail,
            "password": password
        ]

        Alamofire.request(DEV_LOGIN_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                do {
                    guard let responseData = response.data else { return }
                    let json = try JSON(data: responseData)
                    self.userEmail = json["user"].stringValue
                    self.authToken = json["token"].stringValue
                } catch let error as NSError {
                    debugPrint(error as Any)
                }
                self.isLoggedIn = true
                completion(true)
            } else {
                completion(false)
            }
        }
    }

    func createUser(name: String, email: String, avatarName: String, avatarColor: String, completion: @escaping CompletionHandler) {
        let lowercaseEmail = email.lowercased()
        let body = [
            "name": name,
            "email": lowercaseEmail,
            "avatarName": avatarName,
            "avatarColor": avatarColor
        ]

        Alamofire.request(DEV_CREATE_USER_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                    guard let responseData = response.data else { return }
                    self.setUserInfo(data: responseData)
                    completion(true)
            } else {
                completion(false)
            }
        }
    }

    func findUserByEmail(completion: @escaping CompletionHandler) {
        Alamofire.request("\(DEV_FIND_USER_BY_EMAIL_URL)/\(userEmail)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let responseData = response.data else { return }
                self.setUserInfo(data: responseData)
                completion(true)
            } else {
                completion(false)
            }
        }
    }

    func setUserInfo(data: Data) {
        do {
            let json = try JSON(data: data)
            UserDataService.instance.setUserData(_id: json["_id"].stringValue, email: json["email"].stringValue, name: json["name"].stringValue, avatarName: json["avatarName"].stringValue, avatarColor: json["avatarColor"].stringValue)
        } catch let error as NSError {
            debugPrint(error as Any)
        }
    }


}
