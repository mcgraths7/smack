//
//  Channel.swift
//  smack
//
//  Created by Steven McGrath on 9/9/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation

struct Channel {
    public private(set) var channelTitle: String!
    public private(set) var channelDescription: String!
    public private(set) var id: String!
}
