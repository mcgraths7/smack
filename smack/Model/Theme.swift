//
//  Theme.swift
//  smack
//
//  Created by Steven McGrath on 9/7/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation

enum Theme {
    case dark, light
    var color: String {
        switch self {
        case .dark:
            return "dark"
        case .light:
            return "light"
        }
    }
}
