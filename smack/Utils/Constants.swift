//
//  Constants.swift
//  smack
//
//  Created by Steven McGrath on 9/7/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import Foundation

// MARK: Segues
let TO_LOGIN = "toLogin"
let TO_SIGNUP = "toSignup"
let UNWIND = "unwindToChannel"
let TO_AVATARS = "toAvatars"

// MARK: Cells
let AVATAR_CELL = "AvatarCell"
let CHANNEL_CELL = "ChannelCell"
let MESSAGE_CELL = "MessageCell"

// MARK: Development API Info
let DEV_BASE_URL = "https://chattychatchatdev.herokuapp.com/v1/"
let DEV_REGISTER_USER_URL = "\(DEV_BASE_URL)account/register"
let DEV_LOGIN_URL = "\(DEV_BASE_URL)account/login"
let DEV_CREATE_USER_URL = "\(DEV_BASE_URL)user/add"
let DEV_FIND_USER_BY_EMAIL_URL = "\(DEV_BASE_URL)user/byEmail"
let DEV_GET_CHANNELS_URL = "\(DEV_BASE_URL)channel"
let DEV_GET_MESSAGES_URL = "\(DEV_BASE_URL)message/byChannel"

// MARK: API Info
//let BASE_URL = "https://damp-dusk-41188.herokuapp.com/v1"
//let REGISTER__USER_URL = "\(BASE_URL)/account/register"
//let LOGIN_URL = "\(BASE_URL)/account/login"
//let CREATE_USER_URL = "\(BASE_URL)/user/add"
//let FIND_USER_BY_EMAIL_URL = "\(BASE_URL)/user/byEmail"
//let GET_CHANNELS_URL = "\(BASE_URL)/channel"

// MARK: Headers
let HEADER =  ["Content-Type": "application/json; charset=utf-8"]
let BEARER_HEADER = ["Authorization": "Bearer \(AuthService.instance.authToken)", "Content-Type": "application/json; charset=utf-8"]

// MARK: User Defaults
let TOKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL_KEY = "userEmail"

// MARK: Colors
let SMACK_PURPLE_PLACEHOLDER = #colorLiteral(red: 0.3254901961, green: 0.4196078431, blue: 0.7764705882, alpha: 0.5)

// MARK: Notification Center Constants
let NOTIF_USER_DATA_DID_CHANGE = Notification.Name("notifUserDataChanged")
let NOTIF_CHANNELS_LOADED = Notification.Name("channelsLoaded")
let NOTIF_CHANNEL_SELECTED = Notification.Name("channelSelected")



// MARK: Aliases
typealias CompletionHandler = (_ SUCCESS: Bool) -> ()
